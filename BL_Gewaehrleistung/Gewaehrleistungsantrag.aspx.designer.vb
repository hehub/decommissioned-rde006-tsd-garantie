﻿'------------------------------------------------------------------------------
' <automatisch generiert>
'     Der Code wurde von einem Tool generiert.
'
'     Änderungen an der Datei führen möglicherweise zu falschem Verhalten, und sie gehen verloren, wenn
'     der Code erneut generiert wird. 
' </automatisch generiert>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class _Gewaehrleistungsantrag

    '''<summary>
    '''form1-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lblTop-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents lblTop As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''lblDatum-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents lblDatum As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''lblFachhaendler-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents lblFachhaendler As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''txtAntragsteller-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtAntragsteller As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''txtEmail-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtEmail As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''txtLieferscheinnummer-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtLieferscheinnummer As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''txtRechnungsnummer-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtRechnungsnummer As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''datReklamation-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents datReklamation As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''txtModell-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtModell As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''txtModellSeriennummer-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtModellSeriennummer As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''datModellkauf-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents datModellkauf As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''txtZubehoer-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtZubehoer As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''txtZubehoerSeriennummer-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtZubehoerSeriennummer As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''datZubehoerkauf-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents datZubehoerkauf As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''Originalteil-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents Originalteil As Global.DevExpress.Web.ASPxEditors.ASPxRadioButton

    '''<summary>
    '''txtZaehlerstandOriginalSW-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtZaehlerstandOriginalSW As Global.DevExpress.Web.ASPxEditors.ASPxSpinEdit

    '''<summary>
    '''datAusbaudatumOriginalteilSW-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents datAusbaudatumOriginalteilSW As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''txtZaehlerstandOriginalFarbe-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtZaehlerstandOriginalFarbe As Global.DevExpress.Web.ASPxEditors.ASPxSpinEdit

    '''<summary>
    '''datAusbaudatumOriginalteilFarbe-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents datAusbaudatumOriginalteilFarbe As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''Ersatzteil-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents Ersatzteil As Global.DevExpress.Web.ASPxEditors.ASPxRadioButton

    '''<summary>
    '''Verbrauchsmaterial-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents Verbrauchsmaterial As Global.DevExpress.Web.ASPxEditors.ASPxRadioButton

    '''<summary>
    '''txtZaehlerstandErsatzteilEinbauSW-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtZaehlerstandErsatzteilEinbauSW As Global.DevExpress.Web.ASPxEditors.ASPxSpinEdit

    '''<summary>
    '''datEinbaudatumErsatzteilSW-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents datEinbaudatumErsatzteilSW As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''txtZaehlerstandErsatzteilAusbauSW-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtZaehlerstandErsatzteilAusbauSW As Global.DevExpress.Web.ASPxEditors.ASPxSpinEdit

    '''<summary>
    '''datAusbaudatumErsatzteilSW-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents datAusbaudatumErsatzteilSW As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''txtZaehlerstandErsatzteilEinbauFarbe-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtZaehlerstandErsatzteilEinbauFarbe As Global.DevExpress.Web.ASPxEditors.ASPxSpinEdit

    '''<summary>
    '''datEinbaudatumErsatzteilFarbe-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents datEinbaudatumErsatzteilFarbe As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''txtZaehlerstandErsatzteilAusbauFarbe-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtZaehlerstandErsatzteilAusbauFarbe As Global.DevExpress.Web.ASPxEditors.ASPxSpinEdit

    '''<summary>
    '''datAusbaudatumErsatzteilFarbe-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents datAusbaudatumErsatzteilFarbe As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''txtErsatzteilnummer-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtErsatzteilnummer As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''txtErsatzteilbezeichnung-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtErsatzteilbezeichnung As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''txtFehlerbeschreibung-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtFehlerbeschreibung As Global.DevExpress.Web.ASPxEditors.ASPxMemo

    '''<summary>
    '''txtMassnahmen-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtMassnahmen As Global.DevExpress.Web.ASPxEditors.ASPxMemo

    '''<summary>
    '''uplFile-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents uplFile As Global.DevExpress.Web.ASPxUploadControl.ASPxUploadControl

    '''<summary>
    '''btnSend-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents btnSend As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''lblBottom-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents lblBottom As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''txtKundennummer-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtKundennummer As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''txtFachhaendler-Steuerelement
    '''</summary>
    '''<remarks>
    '''Automatisch generiertes Feld
    '''Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    '''</remarks>
    Protected WithEvents txtFachhaendler As Global.DevExpress.Web.ASPxEditors.ASPxTextBox
End Class
