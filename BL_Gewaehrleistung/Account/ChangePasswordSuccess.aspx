﻿<%@ Page Title="Kennwort ändern" Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false"
    CodeBehind="ChangePasswordSuccess.aspx.vb" Inherits="BL_Gewaehrleistung.ChangePasswordSuccess" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Kennwort ändern
    </h2>
    <p>
        Ihr Kennwort wurde geändert.
    </p>
</asp:Content>
