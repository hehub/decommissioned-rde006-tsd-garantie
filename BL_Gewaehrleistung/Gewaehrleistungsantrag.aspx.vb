﻿Imports System.Data.SqlClient

Public Class _Gewaehrleistungsantrag
    Inherits System.Web.UI.Page

    Dim SMTPServer As String = System.Configuration.ConfigurationManager.AppSettings("SMTPServer")
    Dim mailSender As String = System.Configuration.ConfigurationManager.AppSettings("MailSender")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'setControls("huber-ri") 'lokaler Test

            setControls(Request.QueryString("id"))
        End If
    End Sub

    Protected Sub fileUpload()
        Session("resultFilePath1") = "~\_Uploads\" & Now.Ticks & "_" & Me.uplFile.UploadedFiles(0).FileName
        Me.uplFile.UploadedFiles(0).SaveAs(MapPath(Session("resultFilePath1")))
        Session("resultFilePath2") = "~\_Uploads\" & Now.Ticks & "_" & Me.uplFile.UploadedFiles(1).FileName
        Me.uplFile.UploadedFiles(1).SaveAs(MapPath(Session("resultFilePath2")))
        Session("resultFilePath3") = "~\_Uploads\" & Now.Ticks & "_" & Me.uplFile.UploadedFiles(2).FileName
        Me.uplFile.UploadedFiles(1).SaveAs(MapPath(Session("resultFilePath3")))
        Session("resultFilePath4") = "~\_Uploads\" & Now.Ticks & "_" & Me.uplFile.UploadedFiles(3).FileName
        Me.uplFile.UploadedFiles(1).SaveAs(MapPath(Session("resultFilePath4")))
        Session("resultFilePath5") = "~\_Uploads\" & Now.Ticks & "_" & Me.uplFile.UploadedFiles(4).FileName
        Me.uplFile.UploadedFiles(1).SaveAs(MapPath(Session("resultFilePath5")))
        

        'automatische Löschung der Datei vom Server
        UploadingUtils.RemoveFileWithDelay(Me.uplFile.UploadedFiles(0).FileName, MapPath(Session("resultFilePath1")), 10)
        UploadingUtils.RemoveFileWithDelay(Me.uplFile.UploadedFiles(1).FileName, MapPath(Session("resultFilePath2")), 10)
        UploadingUtils.RemoveFileWithDelay(Me.uplFile.UploadedFiles(2).FileName, MapPath(Session("resultFilePath3")), 10)
        UploadingUtils.RemoveFileWithDelay(Me.uplFile.UploadedFiles(3).FileName, MapPath(Session("resultFilePath4")), 10)
        UploadingUtils.RemoveFileWithDelay(Me.uplFile.UploadedFiles(4).FileName, MapPath(Session("resultFilePath5")), 10)

    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        If checkMandatoryFields() Then
            Dim antragsnummer As Integer = 0
            fileUpload()
            insertDB()
            antragsnummer = getRequestnumber()
            sendMail(Me.txtEmail.Text.Trim, antragsnummer)
            sendMailCC(Me.txtEmail.Text.Trim, antragsnummer)
            deleteControls(Me.txtEmail.Text.Trim, antragsnummer)
        End If

    End Sub

    Protected Sub insertDB()
        Dim _con As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("SQLCon"))

        Dim _qry As String = "INSERT INTO tblGewaehrleistungsantraegeFH (intKundennummer, datAntrag, txtAntragsteller, txtEmail, txtFachhaendler, " & _
            " txtLieferscheinnummer, txtReklamationsdatum, txtRechnungsnummer, txtModellbezeichnung, txtModellSeriennummer, " & _
            " txtModellKaufdatum, txtZubehoerbezeichnung, txtZubehoerSeriennummer, txtZubehoerKaufdatum, txtFehlerhaftesTeil, " & _
            " intZaehlerstandOriginalteilSW, txtAusbaudatumOriginalteilSW, intZaehlerstandOriginalteilFarbe, txtAusbaudatumOriginalteilFarbe, " & _
            " intZaehlerstandErsatzteilSW, txtEinbaudatumErsatzteilSW, intZaehlerstandErsatzteilAusbauSW, txtAusbaudatumErsatzteilSW, " & _
            " intZaehlerstandErsatzteilFarbe, txtEinbaudatumErsatzteilFarbe, intZaehlerstandErsatzteilAusbauFarbe, txtAusbaudatumErsatzteilFarbe, " & _
            " txtErsatzteilnummer, txtErsatzteilbezeichnung, txtFehlerbeschreibung)" & _
            " SELECT @kundennummer, @antragsdatum, @antragsteller, @email, @fachhaendler, " & _
            " @lieferscheinnummer, @reklamationsdatum, @rechnungsnummer, @modellbezeichnung, @modellSeriennummer, " & _
            " @modellKaufdatum, @zubehoerbezeichnung, @zubehoerSeriennummer, @zubehoerKaufdatum, @fehlerhaftesTeil, " & _
            " @zaehlerstandOriginalteilSW, @ausbaudatumOriginalteilSW, @zaehlerstandOriginalteilFarbe, @ausbaudatumOriginalteilFarbe, " & _
            " @zaehlerstandErsatzteilEinbauSW, @einbaudatumErsatzteilSW, @zaehlerstandErsatzteilAusbauSW, @ausbaudatumErsatzteilSW, " & _
            " @zaehlerstandErsatzteilEinbauFarbe, @einbaudatumErsatzteilFarbe, @zaehlerstandErsatzteilAusbauFarbe, @ausbaudatumErsatzteilFarbe, " & _
            " @ersatzteilnummer, @ersatzteilbezeichnung, @fehlerbeschreibung"

        Dim _com As New SqlCommand(_qry, _con)

        _com.Parameters.AddWithValue("@kundennummer", Me.txtKundennummer.Text.Trim)
        Session("antragsdatum") = Now()
        _com.Parameters.AddWithValue("@antragsdatum", CDate(Session("antragsdatum")))
        _com.Parameters.AddWithValue("@antragsteller", Me.txtAntragsteller.Text.Trim)
        _com.Parameters.AddWithValue("@email", Me.txtEmail.Text.Trim)
        _com.Parameters.AddWithValue("@fachhaendler", Me.lblFachhaendler.Text.Trim)
        _com.Parameters.AddWithValue("@lieferscheinnummer", Me.txtLieferscheinnummer.Text.Trim)
        _com.Parameters.AddWithValue("@reklamationsdatum", Me.datReklamation.Text.Trim)
        _com.Parameters.AddWithValue("@rechnungsnummer", Me.txtRechnungsnummer.Text.Trim)
        _com.Parameters.AddWithValue("@modellbezeichnung", Me.txtModell.Text.Trim)
        _com.Parameters.AddWithValue("@modellSeriennummer", Me.txtModellSeriennummer.Text.Trim)
        _com.Parameters.AddWithValue("@modellKaufdatum", Me.datModellkauf.Text.Trim)
        _com.Parameters.AddWithValue("@zubehoerbezeichnung", Me.txtZubehoer.Text.Trim)
        _com.Parameters.AddWithValue("@zubehoerSeriennummer", Me.txtZubehoerSeriennummer.Text.Trim)
        _com.Parameters.AddWithValue("@zubehoerKaufdatum", Me.datZubehoerkauf.Text.Trim)

        Dim fehlerhaftesTeil As String = String.Empty
        If Me.Originalteil.Checked Then
            fehlerhaftesTeil = Me.Originalteil.ID.ToString
        End If
        If Me.Ersatzteil.Checked Then
            fehlerhaftesTeil = Me.Ersatzteil.ID.ToString
        End If
        If Me.Verbrauchsmaterial.Checked Then
            fehlerhaftesTeil = Me.Verbrauchsmaterial.ID.ToString
        End If
        _com.Parameters.AddWithValue("@fehlerhaftesTeil", fehlerhaftesTeil)

        _com.Parameters.AddWithValue("@zaehlerstandOriginalteilSW", Me.txtZaehlerstandOriginalSW.Text.Trim)
        _com.Parameters.AddWithValue("@ausbaudatumOriginalteilSW", Me.datAusbaudatumOriginalteilSW.Text.Trim)
        _com.Parameters.AddWithValue("@zaehlerstandOriginalteilFarbe", Me.txtZaehlerstandOriginalFarbe.Text.Trim)
        _com.Parameters.AddWithValue("@ausbaudatumOriginalteilFarbe", Me.datAusbaudatumOriginalteilFarbe.Text.Trim)
        _com.Parameters.AddWithValue("@zaehlerstandErsatzteilEinbauSW", Me.txtZaehlerstandErsatzteilEinbauSW.Text.Trim)
        _com.Parameters.AddWithValue("@einbaudatumErsatzteilSW", Me.datEinbaudatumErsatzteilSW.Text.Trim)
        _com.Parameters.AddWithValue("@zaehlerstandErsatzteilAusbauSW", Me.txtZaehlerstandErsatzteilAusbauSW.Text.Trim)
        _com.Parameters.AddWithValue("@ausbaudatumErsatzteilSW", Me.datAusbaudatumErsatzteilSW.Text.Trim)
        _com.Parameters.AddWithValue("@zaehlerstandErsatzteilEinbauFarbe", Me.txtZaehlerstandErsatzteilEinbauFarbe.Text.Trim)
        _com.Parameters.AddWithValue("@einbaudatumErsatzteilFarbe", Me.datEinbaudatumErsatzteilFarbe.Text.Trim)
        _com.Parameters.AddWithValue("@zaehlerstandErsatzteilAusbauFarbe", Me.txtZaehlerstandErsatzteilAusbauFarbe.Text.Trim)
        _com.Parameters.AddWithValue("@ausbaudatumErsatzteilFarbe", Me.datAusbaudatumErsatzteilFarbe.Text.Trim)
        _com.Parameters.AddWithValue("@ersatzteilnummer", Me.txtErsatzteilnummer.Text.Trim)
        _com.Parameters.AddWithValue("@ersatzteilbezeichnung", Me.txtErsatzteilbezeichnung.Text.Trim)
        _com.Parameters.AddWithValue("@fehlerbeschreibung", Me.txtFehlerbeschreibung.Text.Trim & vbCrLf & vbCrLf & "bereits durchgefuehrte Massnahmen:" & vbCrLf & Me.txtMassnahmen.Text.Trim)

        _con.Open()
        _com.ExecuteNonQuery()
        _con.Close()
    End Sub

    Protected Function getRequestnumber() As Integer
        'suche Antragsnummer
        Dim n As Int32 = 0
        Dim _con As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("SQLCon"))
        Dim _qry As String = "SELECT * FROM tblGewaehrleistungsantraegeFH WHERE datAntrag = @antragsdatum"

        Dim _com As New SqlCommand(_qry, _con)
        _com.Parameters.AddWithValue("@antragsdatum", CDate(Session("antragsdatum")))
        _con.Open()
        Dim _Reader As SqlDataReader = _com.ExecuteReader()
        If _Reader.Read Then
            n = CInt(_Reader.Item("ID"))
        End If
        _con.Close()
        Return n
    End Function

    Protected Function checkMandatoryFields() As Boolean

        Dim ok As Boolean = True
        Me.lblTop.Text = String.Empty
        Me.lblTop.Visible = False
        Me.lblBottom.Text = String.Empty
        Me.lblBottom.Visible = False

        If Me.uplFile.UploadedFiles(0).ContentLength = 0 Then
            ok = False
            Me.lblTop.Text = "Bitte immer eine Datei mitsenden."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte immer eine Datei mitsenden."
            Me.lblBottom.Visible = True
        End If

        If Me.uplFile.UploadedFiles(0).FileName.ToUpper.Contains(System.Configuration.ConfigurationManager.AppSettings("UploadExtensions")) = False Then
            ok = False
            Me.lblTop.Text = "Bitte nur eine PDF-Datei mitsenden."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte nur eine PDF-Datei mitsenden."
            Me.lblBottom.Visible = True
        End If

        If Me.uplFile.UploadedFiles(0).ContentLength > 52428800 Then
            ok = False
            Me.lblTop.Text = "Maximale Dateigröße = 50MB."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Maximale Dateigröße = 50MB."
            Me.lblBottom.Visible = True
        End If

        If String.IsNullOrEmpty(Me.txtMassnahmen.Text) Then
            ok = False
            Me.lblTop.Text = "Bitte bereits durchgeführte Massnahmen eintragen."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte bereits durchgeführte Massnahmen eintragen."
            Me.lblBottom.Visible = True
        End If

        If String.IsNullOrEmpty(Me.txtFehlerbeschreibung.Text) Then
            ok = False
            Me.lblTop.Text = "Bitte möglichst genaue Fehlerbeschreibung eintragen."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte möglichst genaue Fehlerbeschreibung eintragen."
            Me.lblBottom.Visible = True
        End If

        If String.IsNullOrEmpty(Me.txtErsatzteilnummer.Text) Then
            ok = False
            Me.lblTop.Text = "Bitte Ersatzteilnummer eintragen."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte Ersatzteilnummer eintragen."
            Me.lblBottom.Visible = True
        End If

        If Me.Ersatzteil.Checked Or Me.Verbrauchsmaterial.Checked Then
            If String.IsNullOrEmpty(Me.datAusbaudatumErsatzteilSW.Text) Then
                ok = False
                Me.lblTop.Text = "Bitte Ausbaudatum * des Ersatzteils oder Verbrauchsmaterials eintragen."
                Me.lblTop.Visible = True
                Me.lblBottom.Text = "Bitte Ausbaudatum * des Ersatzteils oder Verbrauchsmaterials eintragen."
                Me.lblBottom.Visible = True
            End If
            If String.IsNullOrEmpty(Me.datEinbaudatumErsatzteilSW.Text) Then
                ok = False
                Me.lblTop.Text = "Bitte Einbaudatum * des Ersatzteils oder Verbrauchsmaterials eintragen."
                Me.lblTop.Visible = True
                Me.lblBottom.Text = "Bitte Einbaudatum * des Ersatzteils oder Verbrauchsmaterials eintragen."
                Me.lblBottom.Visible = True
            End If
        End If

        If Me.Originalteil.Checked Then
            If String.IsNullOrEmpty(Me.datAusbaudatumOriginalteilSW.Text) Then
                ok = False
                Me.lblTop.Text = "Bitte Ausbaudatum * des Originalteils eintragen."
                Me.lblTop.Visible = True
                Me.lblBottom.Text = "Bitte Ausbaudatum * des Originalteils eintragen."
                Me.lblBottom.Visible = True
            End If
        End If

        If Me.Originalteil.Checked = False And Me.Ersatzteil.Checked = False And Me.Verbrauchsmaterial.Checked = False Then
            ok = False
            Me.lblTop.Text = "Bitte Angaben zum fehlerhaften Teil eintragen."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte Angaben zum fehlerhaften Teil eintragen."
            Me.lblBottom.Visible = True
        End If

        If String.IsNullOrEmpty(Me.datModellkauf.Text) Then
            ok = False
            Me.lblTop.Text = "Bitte Kaufdatum * des Modells eintragen."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte Kaufdatum * des Modells eintragen."
            Me.lblBottom.Visible = True
        End If

        If String.IsNullOrEmpty(Me.txtModellSeriennummer.Text) Then
            ok = False
            Me.lblTop.Text = "Bitte Seriennummer * des Modells eintragen."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte Seriennummer * des Modells eintragen."
            Me.lblBottom.Visible = True
        End If

        If String.IsNullOrEmpty(Me.txtModell.Text) Then
            ok = False
            Me.lblTop.Text = "Bitte Modellbezeichnung * eintragen."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte Modellbezeichnung * eintragen."
            Me.lblBottom.Visible = True
        End If

        If String.IsNullOrEmpty(Me.txtRechnungsnummer.Text) Then
            ok = False
            Me.lblTop.Text = "Bitte Rechnungsnummer * eintragen."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte Rechnungsnummer * eintragen."
            Me.lblBottom.Visible = True
        End If

        If String.IsNullOrEmpty(Me.datReklamation.Text) Then
            ok = False
            Me.lblTop.Text = "Bitte Reklamationsdatum * eintragen."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte Reklamationsdatum * eintragen."
            Me.lblBottom.Visible = True
        End If

        If String.IsNullOrEmpty(Me.txtLieferscheinnummer.Text) Then
            ok = False
            Me.lblTop.Text = "Bitte Lieferscheinnummer * eintragen."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte Lieferscheinnummer * eintragen."
            Me.lblBottom.Visible = True
        End If

        If String.IsNullOrEmpty(Me.txtEmail.Text) Then
            ok = False
            Me.lblTop.Text = "Bitte Email * eintragen."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte Email * eintragen."
            Me.lblBottom.Visible = True
        End If

        If String.IsNullOrEmpty(Me.txtAntragsteller.Text) Then
            ok = False
            Me.lblTop.Text = "Bitte Antragsteller * eintragen."
            Me.lblTop.Visible = True
            Me.lblBottom.Text = "Bitte Antragsteller * eintragen."
            Me.lblBottom.Visible = True
        End If

        Return ok
    End Function

    Protected Sub deleteControls(ByVal email As String, ByVal antragsnummer As Int32)
        Me.datAusbaudatumErsatzteilFarbe.Text = String.Empty
        Me.datAusbaudatumErsatzteilSW.Text = String.Empty
        Me.datAusbaudatumOriginalteilFarbe.Text = String.Empty
        Me.datAusbaudatumOriginalteilSW.Text = String.Empty
        Me.datEinbaudatumErsatzteilFarbe.Text = String.Empty
        Me.datEinbaudatumErsatzteilSW.Text = String.Empty
        Me.datModellkauf.Text = String.Empty
        Me.datReklamation.Text = String.Empty
        Me.datZubehoerkauf.Text = String.Empty

        Me.txtLieferscheinnummer.Text = String.Empty
        Me.txtErsatzteilbezeichnung.Text = String.Empty
        Me.txtErsatzteilnummer.Text = String.Empty
        Me.txtFehlerbeschreibung.Text = String.Empty
        Me.txtMassnahmen.Text = String.Empty
        Me.txtModell.Text = String.Empty
        Me.txtModellSeriennummer.Text = String.Empty
        Me.txtRechnungsnummer.Text = String.Empty
        Me.txtZaehlerstandErsatzteilAusbauFarbe.Text = String.Empty
        Me.txtZaehlerstandErsatzteilAusbauSW.Text = String.Empty
        Me.txtZaehlerstandErsatzteilEinbauFarbe.Text = String.Empty
        Me.txtZaehlerstandErsatzteilEinbauSW.Text = String.Empty
        Me.txtZaehlerstandOriginalFarbe.Text = String.Empty
        Me.txtZaehlerstandOriginalSW.Text = String.Empty
        Me.txtZubehoer.Text = String.Empty
        Me.txtZubehoerSeriennummer.Text = String.Empty

        Me.Ersatzteil.Checked = False
        Me.Originalteil.Checked = False
        Me.Verbrauchsmaterial.Checked = False

        Me.lblTop.ForeColor = Drawing.Color.DarkGreen
        Me.lblTop.Text = "Vielen Dank, ihre Antragsnummer lautet: " & antragsnummer & vbCrLf & "Der Antrag wird umgehend weiterbearbeitet und eine Bestätigungsemail an - " & email & " - gesendet."
        Me.lblTop.Visible = True
        Me.lblBottom.ForeColor = Drawing.Color.DarkGreen
        Me.lblBottom.Text = "Vielen Dank, ihre Antragsnummer lautet: " & antragsnummer & vbCrLf & "Der Antrag wird umgehend weiterbearbeitet und eine Bestätigungsemail an - " & email & " - gesendet."
        Me.lblBottom.Visible = True

    End Sub

    Protected Sub sendMail(ByVal email As String, ByVal anr As Integer)

        Dim fehlerhaftesTeil As New StringBuilder

        If Me.Originalteil.Checked Then
            fehlerhaftesTeil.Append(Me.Originalteil.ID & ":")
            fehlerhaftesTeil.AppendLine()
            fehlerhaftesTeil.Append("Ausbaudatum-SW: " & Me.datAusbaudatumOriginalteilSW.Text)
            fehlerhaftesTeil.Append(" / Zaehlerstand-SW: " & Me.txtZaehlerstandOriginalSW.Text)
            fehlerhaftesTeil.AppendLine()
            fehlerhaftesTeil.Append("Ausbaudatum-Farbe: " & Me.datAusbaudatumOriginalteilFarbe.Text)
            fehlerhaftesTeil.Append(" / Zaehlerstand-Farbe: " & Me.txtZaehlerstandOriginalFarbe.Text)
        End If

        If Me.Ersatzteil.Checked Or Me.Verbrauchsmaterial.Checked Then
            If Me.Ersatzteil.Checked Then
                fehlerhaftesTeil.Append(Me.Ersatzteil.ID & ":")
            End If
            If Me.Verbrauchsmaterial.Checked Then
                fehlerhaftesTeil.Append(Me.Verbrauchsmaterial.ID & ":")
            End If
            fehlerhaftesTeil.AppendLine()
            fehlerhaftesTeil.Append("Einbaudatum-SW: " & Me.datEinbaudatumErsatzteilSW.Text)
            fehlerhaftesTeil.Append(" / Zaehlerstand-Einbau-SW: " & Me.txtZaehlerstandErsatzteilEinbauSW.Text)
            fehlerhaftesTeil.AppendLine()
            fehlerhaftesTeil.Append("Ausbaudatum-SW: " & Me.datAusbaudatumErsatzteilSW.Text)
            fehlerhaftesTeil.Append(" / Zaehlerstand-Ausbau-SW: " & Me.txtZaehlerstandErsatzteilAusbauSW.Text)
            fehlerhaftesTeil.AppendLine()
            fehlerhaftesTeil.Append("Einbaudatum-Farbe: " & Me.datEinbaudatumErsatzteilFarbe.Text)
            fehlerhaftesTeil.Append(" / Zaehlerstand-Einbau-Farbe: " & Me.txtZaehlerstandErsatzteilEinbauFarbe.Text)
            fehlerhaftesTeil.AppendLine()
            fehlerhaftesTeil.Append("Ausbaudatum-Farbe: " & Me.datAusbaudatumErsatzteilFarbe.Text)
            fehlerhaftesTeil.Append(" / Zaehlerstand-Ausbau-Farbe: " & Me.txtZaehlerstandErsatzteilAusbauFarbe.Text)
        End If

        Dim SmtpObj As New System.Net.Mail.SmtpClient
        Dim MailMessage As New System.Net.Mail.MailMessage()
        SmtpObj.Host = SMTPServer
        'SmtpObj.Port = 25
        MailMessage.From = New System.Net.Mail.MailAddress(mailSender)
        MailMessage.To.Add("gewaehrleistung@ricoh.de")
        MailMessage.Subject = "Ricoh-Gewaehrleistungantrag Nr." & anr
        MailMessage.IsBodyHtml = False
        Dim mailbody As New StringBuilder
        mailbody.Append("GEWAEHRLEISTUNGS - ANTRAGSNUMMER: " & anr)
        mailbody.AppendLine()
        mailbody.Append("vom: " & Session("antragsdatum"))
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("FACHHAENDLERDATEN")
        mailbody.AppendLine()
        mailbody.Append("Antragsteller: " & Me.txtAntragsteller.Text)
        mailbody.AppendLine()
        mailbody.Append("Email: " & email)
        mailbody.AppendLine()
        mailbody.Append("Fachhaendler: " & Me.txtFachhaendler.Text)
        mailbody.AppendLine()
        mailbody.Append("Kundennummer: " & Me.txtKundennummer.Text)
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("Ricoh-Lieferscheinnummer: " & Me.txtLieferscheinnummer.Text)
        mailbody.AppendLine()
        mailbody.Append("Ricoh-Rechnungsnummer: " & Me.txtRechnungsnummer.Text)
        mailbody.AppendLine()
        mailbody.Append("Reklamationsdatum: " & Me.datReklamation.Text)
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("ANGABEN ZUM GERAET")
        mailbody.AppendLine()
        mailbody.Append("Modell: " & Me.txtModell.Text)
        mailbody.AppendLine()
        mailbody.Append("Modell-Seriennummer: " & Me.txtModellSeriennummer.Text)
        mailbody.AppendLine()
        mailbody.Append("Modell-Kaufdatum: " & Me.datModellkauf.Text)
        mailbody.AppendLine()
        mailbody.Append("Zubehoer: " & Me.txtZubehoer.Text)
        mailbody.AppendLine()
        mailbody.Append("Zubehoer-Seriennummer: " & Me.txtZubehoerSeriennummer.Text)
        mailbody.AppendLine()
        mailbody.Append("Zubehoer-Kaufdatum: " & Me.datZubehoerkauf.Text)
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("ANGABEN ZUM FEHLERHAFTEN TEIL")
        mailbody.AppendLine()
        mailbody.Append(fehlerhaftesTeil.ToString)
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("Ersatzteilnummer: " & Me.txtErsatzteilnummer.Text)
        mailbody.AppendLine()
        mailbody.Append("Ersatzteilbezeichnung: " & Me.txtErsatzteilbezeichnung.Text)
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("Fehlerbeschreibung: " & Me.txtFehlerbeschreibung.Text)
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("Bereits durchgefuehrte Massnahmen: " & Me.txtMassnahmen.Text)
        MailMessage.Body = mailbody.ToString

        If Me.uplFile.UploadedFiles(0).ContentLength > 0 Then
            MailMessage.Attachments.Add(New System.Net.Mail.Attachment(MapPath(Session("resultFilePath1"))))
        End If
        If Me.uplFile.UploadedFiles(1).ContentLength > 0 Then
            MailMessage.Attachments.Add(New System.Net.Mail.Attachment(MapPath(Session("resultFilePath2"))))
        End If
        If Me.uplFile.UploadedFiles(2).ContentLength > 0 Then
            MailMessage.Attachments.Add(New System.Net.Mail.Attachment(MapPath(Session("resultFilePath3"))))
        End If
        If Me.uplFile.UploadedFiles(3).ContentLength > 0 Then
            MailMessage.Attachments.Add(New System.Net.Mail.Attachment(MapPath(Session("resultFilePath4"))))
        End If
        If Me.uplFile.UploadedFiles(4).ContentLength > 0 Then
            MailMessage.Attachments.Add(New System.Net.Mail.Attachment(MapPath(Session("resultFilePath5"))))
        End If

        SmtpObj.Send(MailMessage)

    End Sub

    Protected Sub sendMailCC(ByVal email As String, ByVal anr As Integer)
        '' Damit Bestätigungsemail an Fachhändler ohne Anhang gesendet wird

        Dim fehlerhaftesTeil As New StringBuilder

        If Me.Originalteil.Checked Then
            fehlerhaftesTeil.Append(Me.Originalteil.ID & ":")
            fehlerhaftesTeil.AppendLine()
            fehlerhaftesTeil.Append("Ausbaudatum-SW: " & Me.datAusbaudatumOriginalteilSW.Text)
            fehlerhaftesTeil.Append(" / Zaehlerstand-SW: " & Me.txtZaehlerstandOriginalSW.Text)
            fehlerhaftesTeil.AppendLine()
            fehlerhaftesTeil.Append("Ausbaudatum-Farbe: " & Me.datAusbaudatumOriginalteilFarbe.Text)
            fehlerhaftesTeil.Append(" / Zaehlerstand-Farbe: " & Me.txtZaehlerstandOriginalFarbe.Text)
        End If

        If Me.Ersatzteil.Checked Or Me.Verbrauchsmaterial.Checked Then
            If Me.Ersatzteil.Checked Then
                fehlerhaftesTeil.Append(Me.Ersatzteil.ID & ":")
            End If
            If Me.Verbrauchsmaterial.Checked Then
                fehlerhaftesTeil.Append(Me.Verbrauchsmaterial.ID & ":")
            End If
            fehlerhaftesTeil.AppendLine()
            fehlerhaftesTeil.Append("Einbaudatum-SW: " & Me.datEinbaudatumErsatzteilSW.Text)
            fehlerhaftesTeil.Append(" / Zaehlerstand-Einbau-SW: " & Me.txtZaehlerstandErsatzteilEinbauSW.Text)
            fehlerhaftesTeil.AppendLine()
            fehlerhaftesTeil.Append("Ausbaudatum-SW: " & Me.datAusbaudatumErsatzteilSW.Text)
            fehlerhaftesTeil.Append(" / Zaehlerstand-Ausbau-SW: " & Me.txtZaehlerstandErsatzteilAusbauSW.Text)
            fehlerhaftesTeil.AppendLine()
            fehlerhaftesTeil.Append("Einbaudatum-Farbe: " & Me.datEinbaudatumErsatzteilFarbe.Text)
            fehlerhaftesTeil.Append(" / Zaehlerstand-Einbau-Farbe: " & Me.txtZaehlerstandErsatzteilEinbauFarbe.Text)
            fehlerhaftesTeil.AppendLine()
            fehlerhaftesTeil.Append("Ausbaudatum-Farbe: " & Me.datAusbaudatumErsatzteilFarbe.Text)
            fehlerhaftesTeil.Append(" / Zaehlerstand-Ausbau-Farbe: " & Me.txtZaehlerstandErsatzteilAusbauFarbe.Text)
        End If

        Dim SmtpObj As New System.Net.Mail.SmtpClient
        Dim MailMessage As New System.Net.Mail.MailMessage()
        SmtpObj.Host = SMTPServer
        'SmtpObj.Port = 25
        MailMessage.From = New System.Net.Mail.MailAddress(mailSender)
        MailMessage.To.Add(email)
        MailMessage.Subject = "Ricoh-Gewaehrleistungantrag Nr." & anr
        MailMessage.IsBodyHtml = False
        Dim mailbody As New StringBuilder
        mailbody.Append("GEWAEHRLEISTUNGS - ANTRAGSNUMMER: " & anr)
        mailbody.AppendLine()
        mailbody.Append("vom: " & Session("antragsdatum"))
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("FACHHAENDLERDATEN")
        mailbody.AppendLine()
        mailbody.Append("Antragsteller: " & Me.txtAntragsteller.Text)
        mailbody.AppendLine()
        mailbody.Append("Email: " & email)
        mailbody.AppendLine()
        mailbody.Append("Fachhaendler: " & Me.txtFachhaendler.Text)
        mailbody.AppendLine()
        mailbody.Append("Kundennummer: " & Me.txtKundennummer.Text)
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("Ricoh-Lieferscheinnummer: " & Me.txtLieferscheinnummer.Text)
        mailbody.AppendLine()
        mailbody.Append("Ricoh-Rechnungsnummer: " & Me.txtRechnungsnummer.Text)
        mailbody.AppendLine()
        mailbody.Append("Reklamationsdatum: " & Me.datReklamation.Text)
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("ANGABEN ZUM GERAET")
        mailbody.AppendLine()
        mailbody.Append("Modell: " & Me.txtModell.Text)
        mailbody.AppendLine()
        mailbody.Append("Modell-Seriennummer: " & Me.txtModellSeriennummer.Text)
        mailbody.AppendLine()
        mailbody.Append("Modell-Kaufdatum: " & Me.datModellkauf.Text)
        mailbody.AppendLine()
        mailbody.Append("Zubehoer: " & Me.txtZubehoer.Text)
        mailbody.AppendLine()
        mailbody.Append("Zubehoer-Seriennummer: " & Me.txtZubehoerSeriennummer.Text)
        mailbody.AppendLine()
        mailbody.Append("Zubehoer-Kaufdatum: " & Me.datZubehoerkauf.Text)
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("ANGABEN ZUM FEHLERHAFTEN TEIL")
        mailbody.AppendLine()
        mailbody.Append(fehlerhaftesTeil.ToString)
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("Ersatzteilnummer: " & Me.txtErsatzteilnummer.Text)
        mailbody.AppendLine()
        mailbody.Append("Ersatzteilbezeichnung: " & Me.txtErsatzteilbezeichnung.Text)
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("Fehlerbeschreibung: " & Me.txtFehlerbeschreibung.Text)
        mailbody.AppendLine()
        mailbody.AppendLine()
        mailbody.Append("Bereits durchgefuehrte Massnahmen: " & Me.txtMassnahmen.Text)
        MailMessage.Body = mailbody.ToString

        SmtpObj.Send(MailMessage)

    End Sub

    Protected Sub setControls(ByVal userid As String)
        Dim _con As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("SQLCon"))
        Dim _qry As String = "SELECT * FROM tblExtranetIdentdaten WHERE userid = @userid"
        Dim _com As New SqlCommand(_qry, _con)

        Dim param As New SqlParameter("userid", userid)
        _com.Parameters.Add(param)
        _con.Open()

        Dim _reader As SqlDataReader = _com.ExecuteReader()
        If _reader.Read Then
            Me.lblDatum.Text = Date.Today.ToShortDateString
            Me.lblFachhaendler.Text = _reader("BusinessUnit")
            Me.txtAntragsteller.Text = _reader("Username")
            Me.txtEmail.Text = _reader("EMail")
            Me.txtKundennummer.Text = _reader("KundenNr")
            Me.txtFachhaendler.Text = _reader("BusinessUnit")
        End If

        _con.Close()
    End Sub

End Class