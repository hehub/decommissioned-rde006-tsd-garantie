<%@ Page Title="Startseite" Language="vb" AutoEventWireup="false" CodeBehind="Gewaehrleistungsantrag.aspx.vb" Inherits="BL_Gewaehrleistung._Gewaehrleistungsantrag" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<style type="text/css">	
    body { font-family:Arial; }
    td	{ vertical-align:top; font-size:11px; }
	select { font-size:11px; }
	input { font-size:11px; }
	textarea { font-size:11px; }
	.tdStyle { font-size:11px; padding-left:5px; vertical-align:top; }
    .style1
    {
        height: 15px;
    }
    .style2
    {
        font-size: medium;
        color: #CC0000;
    }
    .style3
    {
        text-decoration: underline;
    }
    .style4
    {
        font-size: 11px;
        color: #CC0000;
    }
</style>    
</head>
<body>
<form id="form1" runat="server">

<table border="0" cellpadding="0" cellspacing="1">
    <colgroup>
		<col width=200>
		<col width=10>
		<col width=500>
	</colgroup>
    <tr>
        <td style="text-align:left; padding-top:19px; padding-left:9px;" class="tdStyle">
	  	    <!--img src='http://www2.ricoh.de/extranet/Images/link.gif'><a href="http://www2.ricoh.de/extranet/gewaehrleistungsantragUploadUebersicht.asp">zur Antrags&uuml;bersicht</a-->	  
        </td>
        <td>&nbsp;</td>
        <td style="padding-top:10px;" class="tdStyle">
	          <span style="font-size:20px; font-weight:bold;">Gew&auml;hrleistungsantrag</span>
		      <br><br>
              <dx:ASPxLabel ID="lblTop" runat="server" Font-Bold="False" Font-Size="Small" 
                  ForeColor="Red" Visible="False" Font-Italic="False">
              </dx:ASPxLabel>
              <br><br><br>
        </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Datum</td>
          <td>&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxLabel ID="lblDatum" runat="server" Font-Size="11px">
              </dx:ASPxLabel>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Fachh�ndler</td>
          <td class="tdstyle"></td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxLabel ID="lblFachhaendler" runat="server" Font-Size="11px">
              </dx:ASPxLabel>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Antragsteller</td>
          <td class="tdstyle" style="color:Red;"><strong>*</strong></td>
          <td bgcolor="#eeeeee" class="tdStyle">
              <dx:ASPxTextBox ID="txtAntragsteller" runat="server" Font-Size="11px" 
                  Width="170px">
              </dx:ASPxTextBox>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">eMail</td>
          <td class="tdstyle" style="color:Red;"><strong>*</strong></td>
          <td bgcolor="#eeeeee" class="tdStyle">
              <dx:ASPxTextBox ID="txtEmail" runat="server" Font-Size="11px" 
                  Width="170px">
                  <ValidationSettings SetFocusOnError="True">
                  </ValidationSettings>
              </dx:ASPxTextBox>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;"><hr size="1"></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">RICOH-Lieferscheinnummer</td>
          <td class="tdstyle" style="color:Red;"><strong>*</strong></td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxTextBox ID="txtLieferscheinnummer" runat="server" Font-Size="11px" 
                  Width="170px">
              </dx:ASPxTextBox>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">RICOH-Rechnungsnummer</td>
          <td class="tdstyle" style="color:Red;"><strong>*</strong></td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxTextBox ID="txtRechnungsnummer" runat="server" Font-Size="11px" 
                  Width="170px">
                  <ValidationSettings ValidateOnLeave="False">
                      <RequiredField ErrorText="Bitte Rechnungsnummer eintragen." />
                  </ValidationSettings>
              </dx:ASPxTextBox>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Reklamationsdatum</td>
          <td class="tdstyle" style="color:Red;"><strong>*</strong></td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxDateEdit ID="datReklamation" runat="server" Font-Size="11px" 
                  ClientInstanceName="datReklamation">
                  <ValidationSettings ValidateOnLeave="False">
                      <RequiredField ErrorText="Bitte Reklamationsdatum eintragen." />
                  </ValidationSettings>
              </dx:ASPxDateEdit>
          </td>
    </tr>    
    <tr> 
          <td style="text-align:right;"><hr size="1"></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">&nbsp;</td>
          <td>&nbsp;</td>
          <td><span style="font-size:14px; font-weight:bold;">Angaben zum Modell und 
              Zubeh�r:</span></td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle"><strong>Modellbezeichnung</strong></td>
          <td class="tdstyle" style="color:Red;"><strong>*</strong></td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxTextBox ID="txtModell" runat="server" Font-Size="11px" 
                  Width="170px">
                  <ValidationSettings ValidateOnLeave="False">
                      <RequiredField ErrorText="Bitte Modellbezeichnung eintragen." />
                  </ValidationSettings>
              </dx:ASPxTextBox>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Seriennummer</td>
          <td class="tdstyle" style="color:Red;"><strong>*</strong></td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxTextBox ID="txtModellSeriennummer" runat="server" Font-Size="11px" 
                  Width="170px">
                  <ValidationSettings ValidateOnLeave="False">
                      <RequiredField ErrorText="Bitte Seriennummer des Modells eintragen." />
                  </ValidationSettings>
              </dx:ASPxTextBox>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Kaufdatum</td>
          <td class="tdstyle" style="color:Red;"><strong>*</strong></td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxDateEdit ID="datModellkauf" runat="server" Font-Size="11px">
                  <ValidationSettings ValidateOnLeave="False">
                      <RequiredField ErrorText="Bitte Kaufdatum des Modells eintragen." />
                  </ValidationSettings>
              </dx:ASPxDateEdit>
          </td>
    </tr>
    <tr> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle"><strong>Zubeh�r</strong></td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxTextBox ID="txtZubehoer" runat="server" Font-Size="11px" 
                  Width="170px">
              </dx:ASPxTextBox>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Seriennummer</td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxTextBox ID="txtZubehoerSeriennummer" runat="server" Font-Size="11px" 
                  Width="170px">
              </dx:ASPxTextBox>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Kaufdatum</td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxDateEdit ID="datZubehoerkauf" runat="server" Font-Size="11px">
              </dx:ASPxDateEdit>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;"><hr size="1"></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">&nbsp;</td>
          <td>&nbsp;</td>
          <td><span style="font-size:14px; font-weight:bold;">Angaben zum fehlerhaften Teil: <span style="color:Red;">*</span></span></td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle"><strong>Originalteil</strong></td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxRadioButton ID="Originalteil" runat="server" 
                  Font-Size="11px" GroupName="radGroupFehlerhaftesTeil">
              </dx:ASPxRadioButton>&nbsp;(aus der neuen Maschine/Zubeh&ouml;r)
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Z�hlerstand SW</td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <dx:ASPxSpinEdit ID="txtZaehlerstandOriginalSW" runat="server" 
                            AllowMouseWheel="False" AllowNull="False" Font-Size="11px" MaxValue="100000000" 
                            Number="0" NumberType="Integer" Width="170px">
                            <SpinButtons ShowIncrementButtons="False">
                            </SpinButtons>
                        </dx:ASPxSpinEdit>
                    </td>
                    <td style="padding-right:5px;text-align:right" class="tdstyle" width="100">Ausbaudatum</td>                   
                    <td class="tdstyle">
                        <dx:ASPxDateEdit 
                  ID="datAusbaudatumOriginalteilSW" runat="server">
              </dx:ASPxDateEdit></td>
                </tr>
            </table>        
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Z�hlerstand Farbe</td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <dx:ASPxSpinEdit ID="txtZaehlerstandOriginalFarbe" runat="server" 
                            AllowMouseWheel="False" AllowNull="False" Font-Size="11px" MaxValue="100000000" 
                            Number="0" NumberType="Integer" Width="170px">
                            <SpinButtons ShowIncrementButtons="False">
                            </SpinButtons>
                        </dx:ASPxSpinEdit>
                    </td>
                    <td style="padding-right:5px;text-align:right" class="tdstyle" width="100">Ausbaudatum</td>                   
                    <td class="tdstyle">
                        <dx:ASPxDateEdit 
                  ID="datAusbaudatumOriginalteilFarbe" runat="server">
              </dx:ASPxDateEdit></td>
                </tr>
            </table>        
          </td>
    </tr>
    <tr> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle"><strong>oder Ersatzteil</strong></td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxRadioButton ID="Ersatzteil" runat="server" 
                  Font-Size="11px" GroupName="radGroupFehlerhaftesTeil">
              </dx:ASPxRadioButton>&nbsp;(Spare Part)
          </td>
    </tr>
     <tr> 
          <td style="text-align:right;" class="tdstyle"><strong>oder Verbrauchsmaterial</strong></td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxRadioButton ID="Verbrauchsmaterial" runat="server" 
                  Font-Size="11px" GroupName="radGroupFehlerhaftesTeil">
              </dx:ASPxRadioButton>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Z�hlerstand SW</td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <dx:ASPxSpinEdit ID="txtZaehlerstandErsatzteilEinbauSW" runat="server" 
                            AllowMouseWheel="False" AllowNull="False" Font-Size="11px" MaxValue="100000000" 
                            Number="0" NumberType="Integer" Width="170px">
                            <SpinButtons ShowIncrementButtons="False">
                            </SpinButtons>
                        </dx:ASPxSpinEdit>
                    </td>
                    <td style="padding-right:5px;text-align:right" class="tdstyle" width="100">Einbaudatum</td>                   
                    <td class="tdstyle">
                        <dx:ASPxDateEdit 
                  ID="datEinbaudatumErsatzteilSW" runat="server">
              </dx:ASPxDateEdit></td>
                </tr>
            </table>        
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Z�hlerstand SW</td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <dx:ASPxSpinEdit ID="txtZaehlerstandErsatzteilAusbauSW" runat="server" 
                            AllowMouseWheel="False" AllowNull="False" Font-Size="11px" MaxValue="100000000" 
                            Number="0" NumberType="Integer" Width="170px">
                            <SpinButtons ShowIncrementButtons="False">
                            </SpinButtons>
                        </dx:ASPxSpinEdit>
                    </td>
                    <td style="padding-right:5px;text-align:right" class="tdstyle" width="100">Ausbaudatum</td>                   
                    <td class="tdstyle">
                        <dx:ASPxDateEdit 
                  ID="datAusbaudatumErsatzteilSW" runat="server">
              </dx:ASPxDateEdit></td>
                </tr>
            </table>        
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Z�hlerstand Farbe</td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <dx:ASPxSpinEdit ID="txtZaehlerstandErsatzteilEinbauFarbe" runat="server" 
                            AllowMouseWheel="False" AllowNull="False" Font-Size="11px" MaxValue="100000000" 
                            Number="0" NumberType="Integer" Width="170px">
                            <SpinButtons ShowIncrementButtons="False">
                            </SpinButtons>
                        </dx:ASPxSpinEdit>
                    </td>
                    <td style="padding-right:5px;text-align:right" class="tdstyle" width="100">Einbaudatum</td>                   
                    <td class="tdstyle">
                        <dx:ASPxDateEdit 
                  ID="datEinbaudatumErsatzteilFarbe" runat="server">
              </dx:ASPxDateEdit></td>
                </tr>
            </table>        
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Z�hlerstand Farbe</td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <dx:ASPxSpinEdit ID="txtZaehlerstandErsatzteilAusbauFarbe" runat="server" 
                            AllowMouseWheel="False" AllowNull="False" Font-Size="11px" MaxValue="100000000" 
                            Number="0" NumberType="Integer" Width="170px">
                            <SpinButtons ShowIncrementButtons="False">
                            </SpinButtons>
                        </dx:ASPxSpinEdit>
                    </td>
                    <td style="padding-right:5px;text-align:right" class="tdstyle" width="100">Ausbaudatum</td>                   
                    <td class="tdstyle">
                        <dx:ASPxDateEdit 
                  ID="datAusbaudatumErsatzteilFarbe" runat="server">
              </dx:ASPxDateEdit></td>
                </tr>
            </table>        
          </td>
    </tr>
    <tr> 
          <td class="style1"></td>
          <td class="style1"></td>
          <td class="style1"></td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Ersatzteilnummer</td>
          <td class="tdstyle" style="color:Red;"><strong>*</strong></td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxTextBox ID="txtErsatzteilnummer" runat="server" Font-Size="11px" 
                  Width="170px">
              </dx:ASPxTextBox>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle">Bezeichnung</td>
          <td class="tdstyle" style="color:Red;">&nbsp;</td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxTextBox ID="txtErsatzteilbezeichnung" runat="server" Font-Size="11px" 
                  Width="170px">
              </dx:ASPxTextBox>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;"><hr size="1" style="height: 3px"></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle"><strong>Bitte genaue Fehler- /<br />
              Ursachenbeschreibung</strong><br />
              <br />
              und ggf. Bemerkungsfeld</td>
          <td class="tdstyle" style="color:Red;"><strong>*</strong></td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxMemo ID="txtFehlerbeschreibung" runat="server" Height="100px" 
                  Width="350px">
              </dx:ASPxMemo>
          </td>
    </tr>
    <tr> 
          <td style="text-align:right;" class="tdstyle"><strong>Bereits durchgef�hrte Ma�nahmen</strong></td>
          <td class="tdstyle" style="color:Red;"><strong>*</strong></td>
          <td bgcolor="#eeeeee" class="tdstyle">
              <dx:ASPxMemo ID="txtMassnahmen" runat="server" Height="50px" 
                  Width="350px">
              </dx:ASPxMemo>
          </td>
    </tr>
    <tr> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
    </tr>    
    <tr> 
          <td style="text-align:right;" class="tdstyle"><strong>PDF - Anh�nge</strong><br />
              - Rechnungen<br />
              - Testkopien<br />
              usw.</td>
          <td class="tdstyle" style="color:Red;"><strong>*</strong></td>
          <td>
              <span class="style2">WICHTIG: Immer <span class="style3">komplette</span> 
              SMC-Liste als erstes anf�gen!<br />
              </span>
              <span class="style4"><strong>Wenn m�glich mit dem defekten Ersatzteil die Listen 
              drucken
              (Streifen/Punkte usw.)</strong></span>
              <br />
              <br />
              <dx:ASPxUploadControl ID="uplFile" runat="server" Width="280px" 
                  FileInputCount="5">
                  <ValidationSettings MaxFileSizeErrorText="Maximale Dateigr��e = 50MB"                       
                      NotAllowedFileExtensionErrorText="Dateityp nicht erlaubt !" 
                      AllowedFileExtensions=".pdf, .PDF" 
                      MaxFileSize="52428800">
                      <ErrorStyle Font-Bold="True" Font-Size="X-Small" ForeColor="Red" />
                  </ValidationSettings>
              </dx:ASPxUploadControl>
          </td>
    </tr>
    <tr> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
    </tr>   
    <tr> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><span style="font-size:11px; color:#a70000;">*</span><span style="font-size:11px;"> = Pflichtfelder</span><br>
          <br>Defekte Teile m�ssen <u>innerhalb von vier Wochen</u> bei Ricoh zur Bearbeitung vorliegen. 
          Wird diese Frist schuldhaft vom Vertragsh�ndler nicht eingehalten, bleibt der Antrag unber�cksichtigt. 
          Die Kosten f�r eventuelle R�cksendungen tr�gt der Einsender.<br><br>
          Ungenaue oder fehlende Angaben haben Nachfragen 
          zur Folge und verz&ouml;gern die Bearbeitung unn&ouml;tig. <u>Sind Fehler 
          aus Kopien zu ersehen, m&uuml;ssen entsprechende Testkopien dem Antrag 
          beigef&uuml;gt werden! </u> <br>
          <br>
          <span style="color:#A70000; font-weight:bold;">Antrag bitte vollst&auml;ndig ausf&uuml;llen !!!<br>
          <br>
          Alle zus&auml;tzlichen Informationen ( 
          SMC-Liste, Rechnung, Testkopien ... ) sind dem Antrag als PDF anzuh�ngen.
              <br />
              <br />
              SCM-Liste IMMER an erster Stelle hochladen !<br /><br /> Eingesandte Waren immer sichtbar mit der 
          <span style="color:#A70000; font-weight:bold;">ANTRAGSNUMMER </span> versehen. 
              Ohne Antragsnummer k�nnen die eingesandten Waren nicht zugeordnet werden und 
              m�ssen an den Absender zur&uuml;ckgesendet werden.<br />
              Die Antragsnummer 
          entnehmen sie bitte ihrer Best&auml;tigungsmail !!!</span> <br>
          <br>
              <strong>R&uuml;cksendung bitte adressieren an:</strong><br>
          Ricoh Deutschland GmbH<br>
          Gew&auml;hrleistungsabwicklung<br>
          Product Support<br>
          Vahrenwalder Str. 315<br>
          30179 Hannover<br>
          <br></td>
    </tr>
    <tr> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>
              <dx:ASPxButton ID="btnSend" runat="server" Font-Bold="True" Font-Size="12px" 
                  Text="Gew�hrleistungsantrag jetzt absenden" Width="350px">
              </dx:ASPxButton>
              <br />
              <dx:ASPxLabel ID="lblBottom" runat="server" Font-Bold="False" 
                  Font-Size="Small" ForeColor="Red" Visible="False">
              </dx:ASPxLabel>
              <br />
          </td>
    </tr>
</table>

<dx:ASPxTextBox ID="txtKundennummer" runat="server" ForeColor="#669900" 
    NullText="Kundennummer" Visible="False" Width="100px">
</dx:ASPxTextBox>

<dx:ASPxTextBox ID="txtFachhaendler" runat="server" ForeColor="#669900" 
    NullText="Fachhaendler" Visible="False" Width="100px">
</dx:ASPxTextBox>

</form>
</body>
</html>